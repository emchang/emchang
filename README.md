[![Banner](https://gitlab.com/emchang/emchang/-/raw/main/rss/M._Emily_chang__Animated_.gif)](https://gitlab.com/emchang/emchang/-/blob/main/README.md)

# Emily Chang's README

## 💜 About me

- I go by the name "Em" or "Emily" and my home is 20 minutes away from the bustling city of Kuala Lumpur, Malaysia. 
   - I love travelling and exploring new places. 
   - I also enjoy playing boardgames and online games, here is the [list of online multiplayer games](https://docs.google.com/spreadsheets/d/1G9I_Ksebaw8E-68Lf72zL3SRqJUcVt3BMFZzbNuuu5k/edit#gid=0) for team building (Internal only). 
   - Lately, I'm [learning the Japanese language](https://www.duolingo.com/profile/memilyc). 

- The [GitLab value](https://handbook.gitlab.com/handbook/values/) that resonates with me the most is [🤝 Collaboration](https://handbook.gitlab.com/handbook/values/#collaboration) - although [it's impossible to know everything](hhttps://handbook.gitlab.com/handbook/values/#its-impossible-to-know-everything), I believe we can always learn from each other!

- You can check out [my Curriculum Vitae](https://emchang.gitlab.io/) for a quick summary of my professional career and/or [connect with me on LinkedIn](https://www.linkedin.com/in/memilyc/).

## 🤝 How you can help me

- English is not my first language. Sometimes I have trouble finding the equivalent English word, so please bear with me, and seek clarifications if needed. In Malaysia, we follow the British English, so the spelling of some words may differ.
- I agree with [Negative feedback is 1-1](https://about.gitlab.com/handbook/values/#negative-feedback-is-1-1), my DM is always open to any corrections, please do not feel shy to [provide feedback](https://about.gitlab.com/handbook/values/#give-feedback-effectively) - good or bad. I'm always eager to learn and improve myself.  
- I prefer to have a meeting agenda/outline prior to the call so I know roughly what to expect. Not knowing what to expect may sometimes give me stress. 


## 💼 My working style

- I love asking "Why?" because I prefer to understand the goal and expectations before taking on a task, it's a huge motivation for me if I am aware of the impact. 
- I work well with clear instructions and due dates, as it helps me to prioritise my tasks. 
- I have an eye for detail, I'm not exactly a perfectionist, but I like to double-check before sending. It bugs me when there is a spelling error/grammatical mistake. Please let me know if there are any, I really appreciate it.


## 🤔 What I assume about others

- We both want the best, I’m a huge believer in win-win situations, and I’m willing to compromise. 
- If something is wrong, you are comfortable to let me know. If no one says anything, I will assume you are okay with it. 


## 💬 Communicating with me

- When I work from home, I start my work day from 1AM UTC to 10AM UTC. When travelling, I typically start my work day early in the morning around 11PM UTC, where I catch up with emails and Slack messages and respond to urgent tickets. Then, I continue for deep work from 7AM - 12PM UTC.
- I am open for pairings at any free slot in my calendar where the location is `Home`. 
- I use GitLab To-Do regularly, feel free to ping me on merge requests (MR) or issues.
- I check my Slack and e-mail throughout my working day. Please note that I have desktop notifications turned off when I’m in a meeting. I do not have Slack app on my mobile phone. 
- Slack Channels I visit very often: `#support_gitlab-com`, `#support_self-managed`, `#support_team-chat`, `#support_watercooler`, `#apac`, `#women`, `#allcaps`


## 💪 Strengths

- I consider myself as a creative person, I am not afraid to think out of the box.
- I am friendly and am always happy to lend a pair of listening ears. Please drop me a message if you need someone to talk to or there is anything I can assist with.


## 😩 Weaknesses

- Sometimes, I can be overly optimistic, thus unrealistic. I tend to look for the bright side of things, but I know that it is also good to be prepared for the worst. Please remind me if you’ve noticed this and is affecting my work! 


## 💼 My Work Setup

| Item | Home | Travelling |
| ------ | ------ | ------ |
| [Desk](https://about.gitlab.com/company/culture/all-remote/workspace/#desks) | [Flexispot Electric Height Adjustable Standing Desk](https://www.igreen.my/products/Flexispot-ergonomic-sit-stand-standing-desk-malaysia/malaysia-3-stage-dual-motor-height-adjustable-standing-desk-table) | [Foldable Adjustable Laptop Holder](https://shopee.com.my/AKIRO-Adjustable-Ergonomic-11-17inch-Laptop-Stand-Travel-Size-Komputer-Laptop-Holder-Mount-Office-Home-for-FREE-Pouch-i.19709348.3851868589?sp_atk=4e6fc999-5246-4762-8aec-c056177127ae&xptdk=4e6fc999-5246-4762-8aec-c056177127ae) |
| [Ergonomic Chair](https://about.gitlab.com/company/culture/all-remote/workspace/#chairs) | [Leap Chair](https://store.steelcase.com/seating/office-chairs/leap) | N/A |
| Microphone | [Maono Fairy](https://www.maono.com/products/desktop-usb-microphone-maono-au-903) |  N/A |
| [Headphones](https://about.gitlab.com/company/culture/all-remote/workspace/#headphones) | [HyperX Cloud Alpha headset](https://www.soundguys.com/hyperx-cloud-alpha-gaming-headset-review-23223/) | [AirPods Pro 2](https://www.apple.com/my/airpods-pro/)|
| [External Monitor](https://about.gitlab.com/company/culture/all-remote/workspace/#monitors) | Dell U3818DW |  [I use iPad as a second display for my Mac](https://support.apple.com/en-my/HT210380) |
| [Keyboard](https://about.gitlab.com/company/culture/all-remote/workspace/#external-keyboard-and-mouse) | [Keychron K2v2](https://www.keychron.com/products/keychron-k2-wireless-mechanical-keyboard) | [Logitech K380](https://www.logitech.com/en-my/products/keyboards/k380-multi-device.920-011146.html) |
| [Mouse](https://about.gitlab.com/company/culture/all-remote/workspace/#external-keyboard-and-mouse) | [Logitech MX Master 3](https://www.logitech.com/en-my/products/mice/mx-master-3.910-005698.html) | [Logictech Pebble M350](https://www.logitech.com/en-in/products/mice/m350-pebble-wireless-mouse.910-006666.html) |
| [Webcam](https://about.gitlab.com/company/culture/all-remote/workspace/#webcams) | [Logitech C920](https://www.logitech.com/en-my/products/webcams/c920-pro-hd-webcam.960-000770.html) | N/A |
| USB C Hub | [Ugreen 10 in 1 USB C Hub](https://www.ugreen.com/products/10-in-1-usb-c-hub) | [Ugreen 6-in-1 4K HDMI USB C Hub](https://www.ugreen.com/collections/usb-hub/products/6-in-1-usb-c-adapter-with-4k-hdmi?variant=39915659722814)|

---

<div align='center'>

Thanks for Reading and Feel Free to Connect with Me: 

<a href="https://www.linkedin.com/in/memilyc">
<img src="https://gitlab.com/emchang/emchang/-/raw/main/rss/5282542_linkedin_network_social_network_linkedin_logo_icon.png" width=30px">
</a>
&nbsp;&nbsp;
<a href="https://www.twitter.com/memilychang">
<img src="https://gitlab.com/emchang/emchang/-/raw/main/rss/5282551_tweet_twitter_twitter_logo_icon.png" width=30px>
</a>
</div>
